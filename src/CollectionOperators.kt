/*
 * Created by Adam Bodansky on 2017.11.15..
 */

fun main(args: Array<String>) {

    val numList = 1..20

    val listSum = numList.reduce { x, y -> x + y }
    println("Reduce sum : $listSum")

    val listSum2 = numList.fold(5) { x, y -> x + y }
    println("Fold sum : $listSum2")

    println("Evens : ${numList.any { it % 2 == 0 }}")
    println("Evens : ${numList.all { it % 2 == 0 }}")

    val big3 = numList.filter { it > 3 }

    big3.forEach { println(it) }

    val times7 = numList.map { it * 7 }
    times7.forEach { println(it) }
}