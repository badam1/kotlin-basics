/*
 * Created by Adam Bodansky on 2017.11.15..
 */

fun main(args: Array<String>) {
    println("Sum ${getSum(1, 2, 3, 4, 5)}")

    val multiply = { num1: Int, num2: Int -> num1 * num2 }
    println("5 * 3 = ${multiply(5, 3)}")


    println("5! = ${fact(5)}")
}

fun getSum(vararg nums: Int): Int {
    var sun = 0
    nums.forEach { n -> sun += n }
    return sun
}

fun fact(x: Int): Int {
    tailrec fun factTail(y: Int, z: Int): Int {
        return if (y == 0) z
        else factTail(y - 1, y * z)
    }
    return factTail(x, 1)
}
