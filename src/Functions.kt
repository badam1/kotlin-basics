/*
 * Created by Adam Bodansky on 2017.11.15..
 */

fun main(args: Array<String>) {

    fun add(num1: Int, num2: Int): Int = num1 + num2
    println("1 + 2 : ${add(1, 2)}")

    fun subtract(num1: Int = 1, num2: Int) = num1 - num2
    println("5 - 4 = ${subtract(5, 4)}")

    println("4 -5 = ${subtract(num2 = 5, num1 = 4)}")

    fun sayHello(name: String) = println("Hello $name")
    sayHello("Adam")

    val (two, three) = nextTwo(1)
    println("1 $two $three")
}

fun nextTwo(num: Int): Pair<Int, Int> = Pair(num + 1, num + 2)