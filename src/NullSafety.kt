/*
 * Created by Adam Bodansky on 2017.11.15..
 */

fun main(args: Array<String>) {

    var nullValue: String? = null

    val nullValue1 = returnNull()

    if (nullValue1 != null) {
        println("nullValue1.length}")
    }

    var nullValue3 = nullValue!!.length
    println(nullValue3)

    var nullValue4: String = returnNull() ?: "No name"
}

fun returnNull(): String? = null