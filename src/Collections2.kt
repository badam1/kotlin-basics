/*
 * Created by Adam Bodansky on 2017.11.15..
 */

fun main(args: Array<String>) {

    val map = mutableMapOf<Int, Any?>()

    val map2 = mutableMapOf(1 to "doug", 2 to 25)

    map[1] = "Derek"
    map[2] = 42

    println("Map size : ${map.size}")

    map.put(3, "Pitsburg")
    map.remove(2)

    for ((k, v) in map) {
        println("Key : $k Value : $v")
    }
}