/*
 * Created by Adam Bodansky on 2017.11.15..
 */
import java.util.*

fun main(args: Array<String>) {
    println("Hello World!")

    val bigInt: Int = Int.MAX_VALUE
    val smallInt: Int = Int.MIN_VALUE
    println("Biggest int " + bigInt)
    println("Smallest int $smallInt")

    var myArray = arrayOf(1, 1.34, "Doug")
    println(myArray[2])

    var sqArray = Array(5, { x -> x * x })
    sqArray.forEach { e -> println(e) }

    var intArr: Array<Int> = arrayOf(1, 2, 3, 4, 5)

    // ranges

    val onTo10 = 1..10

    val alpha = "A".."Z"

    println("R in Alpha ${"R" in alpha}")

    val tenTo1 = 10.downTo(1)

    val twoTo20 = 2.rangeTo(20)

    val rng3 = onTo10.step(3)

    for (x in rng3) println("rng3 $x")

    for (x in tenTo1.reversed()) println("Reverse: $x")

    // conditional

    val age = 8

    if (age < 5) {
        println("go to school")
    } else if (age == 5) {
        println("Go to Kindergarten")
    } else if ((age > 5) && (age <= 17)) {
        val grade = age - 5
        println("Go to Grade $grade")
    } else {
        println("Go to College")
    }

    when (age) {
        0, 1, 2, 3, 4 -> println("Go to Preschool")

        5 -> println("Go to Kindergarten")
        in 6..17 -> {
            val grade = age - 5
            println("Go to grade $grade")
        }

        else -> println("Go to college")
    }

    // looping

    for (x in 1..10) {
        println("Loop : $x")
    }

    val rand = Random()
    val magicNum = rand.nextInt(50) + 1

    var guess = 0

    while (magicNum != guess) {
        guess += 1
    }

    println("Magic Number was $guess")

    for (x in 1..20) {
        if (x % 2 == 0) {
            continue
        }
        println("Odd : $x")

        if (x == 15) break
    }

    var array3: Array<Int> = arrayOf(3, 6, 9)
    for (i in array3.indices) {
        println("Mult 3 : ${array3[i]}")
    }

    for ((index, value) in array3.withIndex()) {
        println("Index : $index Value : $value")
    }
}
