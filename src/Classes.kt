/*
 * Created by Adam Bodansky on 2017.11.15..
 */

fun main(args: Array<String>) {


    val bowser = Animal("Bowser", 20.0, 30.5)
    bowser.getInfo()

    val goofy = Dog("Goofy", 45.1, 50.0, "Adam")
    goofy.getInfo()

    val tweedy = Bird("Tweedy", true)
    tweedy.fly(300.25)
}

open class Animal(protected val name: String, protected var height: Double, protected var weight: Double) {

    init {
        val regex = Regex(".*\\d+.*")
        require(!name.matches(regex)) { "Animal name Can't contains numbers" }
        require(height > 0) { "Must be greater then 0" }
        require(weight > 0) { "Must be greater then 0" }
    }

    open fun getInfo() {
        println("$name is $height tall and weights $weight")
    }
}

class Dog(name: String, height: Double, weight: Double, var owner: String) : Animal(name, height, weight) {

    override fun getInfo() {
        println("$name is $height tall and weights $weight and owned by $owner")
    }
}

class Bird constructor(val name: String, override var flies: Boolean = true) : Flyable {
    override fun fly(distMile: Double) {
        if (flies) {
            println("$name flies $distMile")
        }
    }
}

interface Flyable {
    var flies: Boolean
    fun fly(distMile: Double)
}